

#define thermPin A2


// Thermistor Calibration - Thermistor as Pull Up
#define THERM_BETA 3435
#define THERM_T_NAUGHT 298.15
#define THERM_R_NAUGHT 10000
#define THERM_R_REF 10000
const double THERM_R_INF = THERM_R_NAUGHT * pow(2.71828,-((double)THERM_BETA/(double)THERM_T_NAUGHT));

double getTemperature(int raw_ADC) {
    // Display RInf
    // Serial.print("---Using R_Inf as: ");
    // Serial.println(THERM_R_INF);
    // FInd V_OUT
    double vOut;
    vOut = ((double)raw_ADC / 4096.0) * 3.3; // for ESP 4096, for AVR 1024
    // Serial.print("---RAW ADC: ");
    // Serial.println(raw_ADC);
    // Serial.print("---vOUT: ");
    // Serial.println(vOut);
    // Find R_THERM
    double rTherm;
    rTherm = ((3.3 - vOut) * THERM_R_REF) / vOut;
    // Serial.print("---rTherm: ");
    // Serial.println(rTherm);
    // Calculate Temperature
    double temp;
    temp = THERM_BETA / log(rTherm / THERM_R_INF);
    return temp - 273.15;
}

void setup() {
    Serial.begin(9600);
    pinMode(thermPin,INPUT);
}

void loop() {
    Serial.print(millis());
    Serial.print(" - Temperature: ");
    Serial.println(getTemperature(analogRead(thermPin)));
}