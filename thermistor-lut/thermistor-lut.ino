

#define thermPin A2


// Thermistor Calibration - Thermistor as Pull Up
/*
  * Temperature Lookup Table 01 - PC/104 Board Mounted on Rev B
  -Defined for a 10K pull down reference resistor
  -Defined for a 10K NTC Thermistor with a Beta = 3500K
  -Defined for a 3.3VDC supply
*/
#define lookUpTableSize 17
const float temperatures[lookUpTableSize] = {110,100,90,80,70,60,50,40,30,20,10,0,-10,-20,-30,-40,-50};
//const int rawADCValues[lookUpTableSize]  = {953,937,912,881,844,794,729,652,562,462,357,261,177,112,68,37,19}; // For AVR
const int rawADCValues[lookUpTableSize]  = {3812,3748,3648,3524,3376,3176,2916,2608,2248,1848,1428,1044,708,448,272,148,76}; // For ESP32


/**
 * @brief Converts the raw ADC value into the neared degree celsius for onboard thermistors.
 * Uses a temperature look up table defined in the config file
 * @param rawValue Raw ADC Reading (i.e. 0-1023)
 * @return int Nearest degree celsius
 */
int getTemperature(int rawValue) {
  int lowerBound = 0;
  if(rawValue > rawADCValues[0]) {
    return temperatures[0];
  }
  else if(rawValue < rawADCValues[lookUpTableSize-1]) {
    return temperatures[lookUpTableSize-1];
  }
  for(int i = 0; i < lookUpTableSize-1; i++) {
    if(rawValue < rawADCValues[i] && rawValue > rawADCValues[i+1]) {
      lowerBound = i;
    }
  }
  return map(rawValue,rawADCValues[lowerBound+1],rawADCValues[lowerBound],temperatures[lowerBound+1],temperatures[lowerBound]);
}


void setup() {
    Serial.begin(9600);
    pinMode(thermPin,INPUT);
}

void loop() {
    Serial.print(millis());
    Serial.print(" - Temperature: ");
    Serial.println(getTemperature(analogRead(thermPin)));
}